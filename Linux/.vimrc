" This is a basic VIM config file that makes editing code with VI
" a bit easier.
" Copy this file to ~/.vimrc
syntax on
filetype on
set textwidth=0

set hidden

set number

set history=1000         " remember more commands and search history
set undolevels=1000      " use many muchos levels of undo
set wildignore=*.swp,*.bak,*.pyc,*.class
set title                " change the terminal's title
" indenting

set autoindent
set smartindent
set shiftwidth=4

set tabstop=4

set expandtab "turns tabs into whitespace
