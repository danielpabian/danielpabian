import json
import unittest
import unittest.mock
import requests
import weather



class MockResponse:

    def __init__(self, text):
        self.text = text

    def json(self):
        return json.loads(self.text)

    def __iter__(self):
        return self

    def __next__(self):
        return self


def mock_requests_get_success(*args, **kwargs):
    text = """
    {"status":"success",
     "data":{
         "ipv4":"50.225.254.26",
         "continent_name":"North America",
         "country_name":"United States",
         "subdivision_1_name":null,
         "subdivision_2_name":null,
         "city_name":null,
         "latitude":"37.75100",
         "longitude":"-97.82200"}, 
         "currently":{"temperature":"212.52"}}
    """
    response = MockResponse(text)
    return response


class TestLocation(unittest.TestCase):

    @unittest.mock.patch('requests.get', mock_requests_get_success)
    def test_get_location(self):
        longitude, latitude = weather.get_location()
        self.assertEqual("-97.82200", longitude)
        self.assertEqual("37.75100", latitude)


    @unittest.mock.patch('requests.get', mock_requests_get_success)
    def test_get_temperature(self):
        temp = weather.get_temperature("0.0000", "-180.0000")
        self.assertEqual("212.52", temp)



if __name__ == "__main__":
    longitude, latitude = get_location()
    temp = get_temperature(longitude, latitude)
    print_forecast(temp)
