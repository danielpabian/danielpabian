#! /usr/bin/env python3
import requests

DARK_SKY_SECRET_KEY = "d21b17405d692b8977dd9098c59754eb"


def get_location():
    """ Returns the longitude and latitude for the location of this machine.

    Returns:
    str: longitude
    str: latitude
    """
    response = requests.get('https://ipvigilante.com')
    d = response.json()
    longitude = d['data']['longitude']
    latitude = d['data']['latitude']
    return longitude, latitude


def get_temperature(longitude, latitude):
    """
    Returns the current temperature at the specified location
    Parameters:
    longitude (str):
    latitude (str):

    Returns:
    float: temperature
    """
    response = requests.get('https://api.darksky.net/forecast/' + 
                            DARK_SKY_SECRET_KEY + '/' + 
                            latitude + ',' + longitude)
    d = response.json()
    temp = str(d['currently']['temperature'])
    return temp


def print_forecast(temp):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """
    print("Today's forecast is: " + "a temp of " + temp + ' degrees!')


if __name__ == "__main__":
    longitude, latitude = get_location()
    temp = get_temperature(longitude, latitude)
    print_forecast(temp)
